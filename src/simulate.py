#!/usr/bin/env python
import numpy as np
import itertools
import pymc3 as pm
import torch


def gsn_field(kernel_fun, edge_len=32):
    """
    Simulate a 2D Gaussian Process

    :param kernel_fun: A theano kernel function, like those used in the
      pm.gp.cov module
    :param edge_len: The size of the square of the returned gaussian field
    :return z: The edge_len x edge_len torch tensor containing the simulated
      gaussian processes

    Examples
    --------
    kernel_fun = pm.gp.cov.ExpQuad(2, (0.2, 0.2))
    z = gsn_field(kernel_fun)
    plt.imshow(z)
    """
    u = np.linspace(0, 1, edge_len)
    x = np.array(list(itertools.product(u, u)))

    K = kernel_fun(x).eval()
    z = pm.MvNormal.dist(mu=np.zeros(K.shape[0]), cov=K).random(size=1)
    return torch.Tensor(z.reshape(edge_len, edge_len))


def cat_field(kernel_fun, k_cat=3, edge_len=32):
    """
    Simulate Dice Throws according to Gaussian Process

    This applies a softmax to a series of gaussian processes, to get a field of
    categorical probabilities.

    :param kernel_fun: A theano kernel function, like those used in the
      pm.gp.cov module
    :param k_cat: The number of sides to the dice to throw at each spatial
      pixel.
    :param edge_len: The size of the square of the returned gaussian field
    :return A tuple with two elements,
       - sample -- A torch tensor of a sample from the specified process.
       - cat -- The underlying pytorch categorial distribution which gave rise
         to the observed sample.

    """
    log_pi = stacked_fields(kernel_fun, edge_len, k_cat)
    pi =  torch.nn.Softmax()(log_pi.transpose(0, 2))
    pi = pi.transpose(0, 2)

    cat = torch.distributions.categorical.Categorical(pi)
    return cat.sample(), cat


def stacked_fields(kernel_fun, edge_len=32, k_layers=4):
    """
    Stack Independent 2D Gaussian Processes

    :param kernel_fun: A theano kernel function, like those used in the
      pm.gp.cov module
    :param edge_len: The size of the square of the returned gaussian field
    :param k_layers: How many independent processes should be simulated?

    :return z: An edge_len x edge_len x k_layers numpy array containing the
      simulated gaussian processes, each stacked on top of the other.
    """
    z = torch.zeros((edge_len, edge_len, k_layers))
    for k in range(k_layers):
        z[:, :, k] = gsn_field(kernel_fun, edge_len)

    return z
