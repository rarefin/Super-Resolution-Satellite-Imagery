import torch
import numpy as np


class ToTensor(object):
    """Convert a numpy array to tensor.

    Converts a numpy array (H x W) in the range
    to a torch.FloatTensor of shape (1 x H x W).
    """
    def __call__(self, image, axis=0):
        """
        :param image: Image to be converted to tensor.
        :return: Converted image
        """
        if isinstance(image, np.ndarray):
            # img = torch.from_numpy(np.expand_dims(image.astype(np.float32), axis=axis))
            img = torch.from_numpy(image.astype(np.float32))

            return img.float()


class MinMaxNormalize(object):
    """Convert a numpy array in the range [0, 65536] to the range [0.0, 1.0] using min-max normalization
    """
    def __init__(self, min=0, max=65536):
        self.min = min
        self.max = max

    def __call__(self, image, axis=0):
        """
        :param image: Image to be scaled to 0~1
        :return: Scaled image
        """
        image.sub_(self.min).div_(self.max - self.min)

        return image



class Standardize(object):
    """ Standardize a numpy array to zero mean and unit variance
    """
    def __init__(self, mu, std):
        self.mu = mu
        self.std = std

    def __call__(self, image):
        """
        :param image: Image to be standardized
        :return: Scaled image
        """
        image.sub_(self.mu).div_(self.std)

        return image