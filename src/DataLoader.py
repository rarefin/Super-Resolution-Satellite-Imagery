from torch.utils.data import Dataset
from os.path import join
from utils import readImage, traditionalUpscaling
import numpy as np
import glob
import random
import torch


def getRandomPatch(img, start_x, start_y, patch_size=32):
    """
        Slice a random patch from a given image
    :param img: Input image
    :param start_x: Start x position of the random patch
    :param start_y: Start y position of the random patch
    :param patch_size: width/height of the patch
    :return: random patch
    """

    return img[start_x:start_x + patch_size, start_y:start_y + patch_size]


def readAllImages(imageset_directory, create_patches=False, patch_size=64):
    """

    :param imageset_directory: imageset directory
    :param create_patches: should we sample patch or return full image
    :param lr_window_size: Lower res patch size
    :return: low  and higher res images and status maps of lower res and higher res images

    """

    # retrieving the names of all low res images in an imageset directory
    indices = [path[-7:-4] for path in glob.glob(imageset_directory + '/QM*.png')]
    # reading all low res images
    low_res_images = [readImage(join(imageset_directory, 'LR' + index + '.png')) for index in indices]

    low_res_status_maps = [readImage(join(imageset_directory, 'QM' + index + '.png')) / 255.
                           for index in indices]

    if create_patches:
        max_range = low_res_images[0].shape[0] - patch_size
        x = np.random.randint(max_range)
        y = np.random.randint(max_range)
        for i in range(len(low_res_images)):
            low_res_images[i] = getRandomPatch(low_res_images[i], x, y, patch_size)
            low_res_status_maps[i] = getRandomPatch(low_res_status_maps[i], x, y, patch_size)

    try:
        # reading high res image and status map
        high_res_image = readImage(join(imageset_directory, 'HR.png'))
        high_res_status_map = readImage(join(imageset_directory, 'SM.png'))
        high_res_status_map = high_res_status_map / 255.

        if create_patches:
            x *= 3
            y *= 3
            patch_size *= 3
            high_res_image = getRandomPatch(high_res_image, x, y, patch_size)
            high_res_status_map = getRandomPatch(high_res_status_map, x, y, patch_size)
    except:
        # Test data, so no high res image
        high_res_image = None

    return low_res_images, low_res_status_maps, high_res_image, high_res_status_map


def getClearestLowRes(low_res_images, low_res_status_maps):
    """

    :param low_res_images: all low res image of an image set
    :param low_res_status_maps: all low res staus map of an image set
    :return: most clearest low res image and status map
    """

    # find the most clearest low res image best on status map
    clearance_scores = np.sum(low_res_status_maps, axis=(1, 2))
    clearest_index = np.argmax(clearance_scores)

    # getting clearest image
    clearest_low_res_image = low_res_images[clearest_index]
    clearest_low_res_status = low_res_status_maps[clearest_index]

    return clearest_low_res_image, clearest_low_res_status


def getRandomLowRes(low_res_images, low_res_status_maps):
    """

    :param low_res_images: all low res image of an image set
    :param low_res_status_maps: all low res staus map of an image set
    :return: get random low res image and status map

    """
    # selecting a random low res index
    random_index = random.randint(0, low_res_images.shape[0])

    # reshaping to 3d numpy array
    random_low_res_image = low_res_images[random_index]
    random_low_res_status = low_res_status_maps[random_index]

    return random_low_res_image, random_low_res_status


class BaseDataset(Dataset):
    """
        Base Dataset Class which return all the low res image, low res status map and high res image from a directory
        along with image set directory name
    """

    def __init__(self, image_set_directories, create_patches=False, patch_size=None):
        super(BaseDataset, self).__init__()
        self.image_set_directories = image_set_directories
        self.create_patches = create_patches
        self.patch_size = patch_size

    def __len__(self):
        return len(self.image_set_directories)

    def __getitem__(self, index):
        # retrieving image set name
        image_set_name = self.image_set_directories[index].split("/")[-1]

        low_res_images, low_res_status_maps, high_res_image, high_res_status_map = readAllImages(
            self.image_set_directories[index], self.create_patches, self.patch_size)

        return low_res_images, low_res_status_maps, high_res_image, high_res_status_map, image_set_name

class SingleImageDataset(BaseDataset):
    """
        Derived Dataset class for loading single low res image it can be either random or most clearest one based on
        the status map
    """

    def __init__(self, image_set_directories, isClearest=True, transform=None):
        super(SingleImageDataset, self).__init__(image_set_directories)
        self.isClearest = isClearest
        self.transform = transform

    def __getitem__(self, index):
        low_res_all, low_status_all, high_res_image, \
        high_res_status_map, image_set_name = super(SingleImageDataset, self).__getitem__(index)

        if self.isClearest:
            low_res_image, low_res_status_map = getClearestLowRes(low_res_all, low_status_all)
        else:
            low_res_image, low_res_status_map = getRandomLowRes(low_res_all, low_status_all)

        #         low_res_all = low_res_all * low_status_all
        #         low_res_status_map = low_status_all[0]
        #         low_res_image = viewPool(low_res_all)
        #         low_res_image = np.mean(low_res_all, axis=0)

        #         low_res_image = low_res_image * low_res_status_map
        # creating upscaled version of low res image using bicubic interpolation
        bicubic_upscaled_image = traditionalUpscaling(low_res_image, interpolation_method='BiCubic',
                                                      upscaling_factor=3)
        #         high_res_image = high_res_image * high_res_status_map

        #         low_res_image = scale_percentile(low_res_image)
        #         bicubic_upscaled_image = scale_percentile(bicubic_upscaled_image)

        #         low_res_image = scale_low(low_res_image)
        #         bicubic_upscaled_image = scale_low(bicubic_upscaled_image)
        #         high_res_image = scale_high(high_res_image)

        if self.transform is not None:
            low_res_image = self.transform(low_res_image)
            bicubic_upscaled_image = self.transform(bicubic_upscaled_image)

        if high_res_image is not None:
            high_res_image = self.transform(high_res_image)
            high_res_status_map = torch.from_numpy(np.expand_dims(high_res_status_map.astype(np.float32), axis=0))
        else:
            high_res_image = 0
            high_res_status_map = 0

        #         high_res_image = torch.from_numpy(np.expand_dims(high_res_image, axis=0))

        #         low_res_image = torch.from_numpy(np.expand_dims(low_res_image, axis=0))
        #         bicubic_upscaled_image = torch.from_numpy(np.expand_dims(bicubic_upscaled_image, axis=0))

        low_res_status_map = torch.from_numpy(np.expand_dims(low_res_status_map.astype(np.float32), axis=0))

        return low_res_image, bicubic_upscaled_image, low_res_status_map, high_res_image, high_res_status_map, image_set_name


class AllImageDataset(BaseDataset):
    """
        Derived Dataset class for loading all low res images

    """
    def __init__(self, image_set_directories, config, transform=None):
        super(AllImageDataset, self).__init__(image_set_directories, config["create_patches"], config["patch_size"])
        self.transform = transform

    def __getitem__(self, index):
        low_res_all, low_status_all, high_res_image, high_res_status_map, \
        image_set_name = super(AllImageDataset, self).__getitem__(index)

        n_low_res = len(low_res_all)
        low_res_all = np.array(low_res_all)
        low_status_all = np.array(low_status_all)

        # creating upscaled version of low res images using bicubic interpolation
        bicubic_upscaled_all = []
        for i, low_res in enumerate(low_res_all):
            bicubic_upscaled = traditionalUpscaling(low_res, interpolation_method='BiCubic',
                                                    upscaling_factor=3)
            bicubic_upscaled_all.append(bicubic_upscaled)
        bicubic_upscaled_all = np.array(bicubic_upscaled_all)

        low_res_all = np.expand_dims(low_res_all, axis=1)
        low_status_all = np.expand_dims(low_status_all, axis=1)
        bicubic_upscaled_all = np.expand_dims(bicubic_upscaled_all, axis=1)
        high_res_status_map = np.expand_dims(high_res_status_map, axis=0)

        if high_res_image is not None:
            high_res_image = np.expand_dims(high_res_image, axis=0)

        if self.transform is not None:
            low_res_all = self.transform(low_res_all)
            bicubic_upscaled_all = self.transform(bicubic_upscaled_all)

            if high_res_image is not None:
                high_res_image = np.expand_dims(high_res_image, axis=0)
            else:
                high_res_image = 0


        low_status_all = torch.from_numpy(low_status_all.astype(np.float32))
        high_res_status_map = torch.from_numpy(high_res_status_map.astype(np.float32))

        return low_res_all, bicubic_upscaled_all, low_status_all, high_res_image, high_res_status_map, n_low_res, image_set_name


class ImageDataSubset(BaseDataset):
    """
        Derived Dataset class for loading subset of all low res images

    """

    def __init__(self, image_set_directories, config, transform=None):
        super(ImageDataSubset, self).__init__(image_set_directories, config["create_patches"], config["patch_size"])
        self.transform = transform
        self.num_low_res = config["num_low_res"]

    def __getitem__(self, index):
        low_res_all, low_status_all, high_res_image, high_res_status_map, \
        image_set_name = super(ImageDataSubset, self).__getitem__(index)

        n_low_res = len(low_res_all)
        if self.num_low_res > n_low_res:
            for _ in range(self.num_low_res - len(low_res_all)):
                low_res_all.append(np.zeros(low_res_all[0].shape))
                low_status_all.append(np.zeros(low_status_all[0].shape))
        else:
            n_low_res = self.num_low_res
        low_res_all = np.array(low_res_all)
        low_status_all = np.array(low_status_all)

        indices = [i for i in range(len(low_res_all))]
        random.shuffle(indices)
        low_res_all = low_res_all[indices[0:self.num_low_res]]
        low_status_all = low_status_all[indices[0:self.num_low_res]]

        # creating upscaled version of low res images using bicubic interpolation
        bicubic_upscaled_all = []
        for i, low_res in enumerate(low_res_all):
            bicubic_upscaled = traditionalUpscaling(low_res, interpolation_method='BiCubic',
                                                    upscaling_factor=3)
            bicubic_upscaled_all.append(bicubic_upscaled)
        bicubic_upscaled_all = np.array(bicubic_upscaled_all)

        low_res_all = np.expand_dims(low_res_all, axis=1)
        low_status_all = np.expand_dims(low_status_all, axis=1)
        bicubic_upscaled_all = np.expand_dims(bicubic_upscaled_all, axis=1)
        high_res_status_map = np.expand_dims(high_res_status_map, axis=0)

        if high_res_image is not None:
            high_res_image = np.expand_dims(high_res_image, axis=0)

        if self.transform is not None:
            low_res_all = self.transform(low_res_all)
            bicubic_upscaled_all = self.transform(bicubic_upscaled_all)

            if high_res_image is not None:
                high_res_image = self.transform(high_res_image)
            else:
                high_res_image = 0

        low_status_all = torch.from_numpy(low_status_all.astype(np.float32))
        high_res_status_map = torch.from_numpy(high_res_status_map.astype(np.float32))

        return low_res_all, bicubic_upscaled_all, low_status_all, high_res_image, high_res_status_map, n_low_res, image_set_name