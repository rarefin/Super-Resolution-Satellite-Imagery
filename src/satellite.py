#!/usr/bin/env python

"""
Utiltiies for obtaining satellite data
"""
from osgeo import gdal
from requests.auth import HTTPBasicAuth
from retrying import retry
import os
import os.path
import re
import json
import requests
import shutil


def search_pages(geometry, start=None, end=None, api_key=None, max_links=100):
    """
    Links to / Metadata for raw Planet API Images

    :param geometry: A geojson geometry giving a polygon within which to
      restrict the search for images.
    :param start: The datetime (as a string) giving the earliest acquisition
      time for any of the images we want.
    :param end: The datetime (as a string) giving the latest acquisition time
      for any of the images we want.
    :param api_key: The Planet API key that authorizes our request.
    :return result: A json with three keys, '_links', 'features', and 'type'.
      _links gives the paths that we'll need to activate in order to download
      images. 'features' gives us metadata describing each image (e.g., it's
      bounding box, and the satellite it was taken from).

    >>> geo_json_geometry = {
    >>>     "type": "Polygon",
    >>>     "coordinates": [
    >>>         [
    >>>             [-118.38317871093749, 34.05379721731628],
    >>>             [-118.21495056152342, 34.05379721731628],
    >>>             [-118.21495056152342, 34.11748941036342],
    >>>             [-118.38317871093749, 34.11748941036342],
    >>>             [-118.38317871093749, 34.05379721731628]
    >>>         ]
    >>>         ]
    >>>     }
    >>> result = search_pages(geo_json_geometry, api_key=api_key)
    """
    # fill in some defaults
    if not start:
        start = "2017-10-31T00:00:00.000Z"
    if not end:
        end = "2018-10-31T00:00:00.000Z"

    # define the filters
    geometry_filter = {
        "type": "GeometryFilter",
        "field_name": "geometry",
        "config": geometry
    }

    date_range_filter = {
        "type": "DateRangeFilter",
        "field_name": "acquired",
        "config": {"gte": start, "lte": end}
    }

    complete_filter = {
        "type": "AndFilter",
        "config": [geometry_filter, date_range_filter]
    }

    # make the endpoint request
    endpoint_request = {
        "item_types": ["PSScene3Band"],
        "filter": complete_filter
    }

    results = [
        requests.post(
            "https://api.planet.com/data/v1/quick-search",
            auth=HTTPBasicAuth(api_key, ""),
            json=endpoint_request
        ).json()
    ]

    links = []
    while results[-1]["_links"].get("_next"):
        cur_link = results[-1]["_links"].get("_next")

        if cur_link not in links:
            print("activating {}".format(cur_link))
            results.append(
                requests.get(
                    cur_link,
                    auth=HTTPBasicAuth(api_key, "")
                ).json()
            )
            links.append(cur_link)

        if len(links) >= max_links:
            break

    return results


def bbox_tiff(path):
    """
    Get Bounding Box from a Tiff
    """
    data = gdal.Open(path)
    ulx, xres, xskew, uly, yskew, yres  = data.GetGeoTransform()
    lrx = ulx + (data.RasterXSize * xres)
    lry = uly + (data.RasterYSize * yres)
    return [[ulx, uly], [ulx, lry], [lrx, lry], [lrx, uly]]


def clip_request(bbox, item_id, api_key, item_type="PSScene3Band",
                 asset_type="visual",
                 cas_url="https://api.planet.com/compute/ops/clips/v1/"):
    """
    Get Resposne from AOI API

    Examples
    --------
    >>> path = "/Users/krissankaran/Desktop/SpaceNet_Buildings_Competition_Round2_Sample/AOI_5_Khartoum_Train/PAN/PAN_AOI_5_Khartoum_img1232.tif"
    >>> bbox = bbox_tiff(path)
    >>> aoi = {"type": "Polygon", "coordinates": [bbox + [bbox[0]]]}
    >>> ids = search_pages(aoi, api_key=api_key)
    >>>
    >>> item_id = ids[0]["features"][0]["id"]
    >>> clip = clip_request(bbox, item_id, api_key)
    """
    aoi = {
        "aoi": {
            "type": "Polygon",
            "coordinates": [bbox + [bbox[0]]]
        },
        "targets": [{
            "item_id": item_id,
            "item_type": item_type,
            "asset_type": asset_type
        }]
    }

    headers = {'Content-type': 'application/json'}
    return requests.post(
        cas_url,
        data=json.dumps(aoi),
        headers=headers,
        auth=HTTPBasicAuth(api_key, "")
    )


@retry(stop_max_attempt_number=20, wait_fixed=6e4)
def download_clip(response, api_key, output_name=None):
    """
    Download files from AOI API Response

    Examples
    --------
    >>> clip = clip_request(bbox, item_id, api_key)
    >>> download_clip(clip, api_key)
    """
    # get link to the packaged clip data
    r = requests.get(
        response.json()['_links']['_self'],
        auth=HTTPBasicAuth(api_key, "")
    )
    print("Attempting to fetch " + r.text + "...")

    # keep looking for clip data at that link
    result = requests.get(
        r.json()["_links"]["results"][0],
        auth=HTTPBasicAuth(api_key, ""),
        stream=True
    )

    # download that data
    if not output_name:
        output_name = response.json()["id"] + ".zip"

    result.raw.decode_content = True
    with open(output_name, "wb") as out_file:
        shutil.copyfileobj(result.raw, out_file)

    return output_name
