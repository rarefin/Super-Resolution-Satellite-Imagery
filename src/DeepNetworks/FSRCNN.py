from torch import nn
import torch


class FSRCNN(nn.Module):
    def __init__(self, in_channels, out_channels, d=56, s=12, m=4):
        # too big network may leads to over-fitting
        super(FSRCNN, self).__init__()

        # Feature extraction
        self.first_part = nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=d, kernel_size=3, stride=1, padding=0),
            nn.PReLU())
        self.layers = []
        # Shrinking
        self.layers.append(nn.Sequential(nn.Conv2d(in_channels=d, out_channels=s, kernel_size=1, stride=1, padding=0),
                                         nn.PReLU()))

        # Non-linear Mapping
        for _ in range(m):
            self.layers.append(
                nn.Sequential(nn.Conv2d(in_channels=s, out_channels=s, kernel_size=3, stride=1, padding=1),
                              nn.PReLU()))

        # # Expanding
        self.layers.append(nn.Sequential(nn.Conv2d(in_channels=s, out_channels=d, kernel_size=1, stride=1, padding=0),
                                         nn.PReLU()))

        self.mid_part = nn.Sequential(*self.layers)

        # Deconvolution
        self.last_part = nn.ConvTranspose2d(in_channels=d, out_channels=out_channels, kernel_size=9, stride=3,
                                            padding=0, output_padding=0)

    def forward(self, x):
        #         low_res_batch, bicubic_upscaled_batch, low_res_status_batch = x
        out = self.first_part(x)
        out = self.mid_part(out)
        out = self.last_part(out)

        return out

    def weight_init(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
                # m.weight.data.normal_(0.0, 0.2)
                m.weight.data.normal_(0.0, sqrt(2 / m.out_channels / m.kernel_size[0] / m.kernel_size[0]))  # MSRA
                # nn.init.xavier_normal(m.weight) # Xavier
                if m.bias is not None:
                    m.bias.data.zero_()


class Encoder(nn.Module):
    def __init__(self, in_channels, out_channels, d=56, s=12, m=4):
        super(Encoder, self).__init__()

        # Feature extraction
        self.first_part = nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=d, kernel_size=3, stride=1, padding=0),
            nn.PReLU())
        self.layers = []
        # Shrinking
        self.layers.append(nn.Sequential(nn.Conv2d(in_channels=d, out_channels=s, kernel_size=1, stride=1, padding=0),
                                         nn.PReLU()))

        # Non-linear Mapping
        for _ in range(m):
            self.layers.append(
                nn.Sequential(nn.Conv2d(in_channels=s, out_channels=s, kernel_size=3, stride=1, padding=1),
                              nn.PReLU()))

        self.mid_part = nn.Sequential(*self.layers)

    def forward(self, x):
        out = self.first_part(x)
        out = self.mid_part(out)
        return out

    def weight_init(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
                # m.weight.data.normal_(0.0, 0.2)
                m.weight.data.normal_(0.0, sqrt(2 / m.out_channels / m.kernel_size[0] / m.kernel_size[0]))  # MSRA
                # nn.init.xavier_normal(m.weight) # Xavier
                if m.bias is not None:
                    m.bias.data.zero_()


class Decoder(nn.Module):
    def __init__(self, in_channels, out_channels, d=56, s=12, m=4):
        # too big network may leads to over-fitting
        super(Decoder, self).__init__()
        # Deconvolution
        self.last_part = nn.ConvTranspose2d(in_channels=d, out_channels=d, kernel_size=9, stride=3,
                                            padding=0, output_padding=0)

    def forward(self, x):
        out = self.last_part(x)
        return out

    def weight_init(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
                # m.weight.data.normal_(0.0, 0.2)
                m.weight.data.normal_(0.0, sqrt(2 / m.out_channels / m.kernel_size[0] / m.kernel_size[0]))  # MSRA
                # nn.init.xavier_normal(m.weight) # Xavier
                if m.bias is not None:
                    m.bias.data.zero_()


class FSRCNNMultiView(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(FSRCNNMultiView, self).__init__()
        self.weights = FSRCNN(in_channels, out_channels)
        self.fsrcnn = Encoder(in_channels, out_channels)
        self.concat1 = nn.Sequential(nn.Conv2d(in_channels=20, out_channels=56, kernel_size=1, stride=1, padding=0),
                                 nn.PReLU())

        self.LRStatus = nn.Sequential(nn.Conv2d(in_channels=1, out_channels=4, kernel_size=3, stride=1, padding=0),
                                      nn.PReLU(),
                                      nn.Conv2d(in_channels=4, out_channels=8, kernel_size=3, stride=1, padding=1),
                                      nn.PReLU(),
                                      )

        self.concat2 = nn.Sequential(nn.Conv2d(in_channels=72, out_channels=1, kernel_size=1, stride=1, padding=0),
                                  nn.PReLU())
        self.decode = Decoder(in_channels, out_channels)

        self.HRStatus = nn.Sequential(nn.Conv2d(in_channels=1, out_channels=8, kernel_size=3, stride=1, padding=1),
                                      nn.PReLU(),
                                      nn.Conv2d(in_channels=8, out_channels=16, kernel_size=3, stride=1, padding=1),
                                      nn.PReLU(),
                                      )

    def forward(self, x):
        low_res_batch, bicubic_upscaled_batch, low_res_status_batch, high_res_status_batch = x

        out = []
        st = self.HRStatus(high_res_status_batch)
        for i, x in enumerate(low_res_batch):
            x = torch.cat([x, low_res_status_batch[i]], 1)

            weights = self.weights(x)
            weights = torch.softmax(weights, dim=0)
            x = self.fsrcnn(x)
            c = self.LRStatus(low_res_status_batch[i])
            x = torch.cat((x, c), 1)

            x = self.concat1(x)

            x = self.decode(x)

            x = torch.cat((x, st[i].repeat(x.size()[0], 1, 1, 1)), 1)
            x = self.concat2(x)
            x = torch.sum(weights * x, dim=0)

            out.append(x)

        out = torch.stack(out, 0)

        return out