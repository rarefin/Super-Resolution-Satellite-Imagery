from torch import nn
import torch


class SRCNN(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(SRCNN, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, 64, kernel_size=9, padding=4)
        self.relu1 = nn.ReLU()
        self.conv2 = nn.Conv2d(64, 32, kernel_size=1, padding=0)
        self.relu2 = nn.ReLU()
        self.conv3 = nn.Conv2d(32, out_channels, kernel_size=5, padding=2)

    def forward(self, x):

        out = self.conv1(x)
        out = self.relu1(out)
        out = self.conv2(out)
        out = self.relu2(out)
        out = self.conv3(out)

        # out = (torch.tanh(out) + 1) / 2

        return out


class SRCNNMultiView(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(SRCNNMultiView, self).__init__()
        self.srcnn = SRCNN(in_channels, out_channels)

    def forward(self, x):
        low_res_batch, bicubic_upscaled_batch, low_res_status_batch, high_res_status_batch = x

        out = []
        for i, x in enumerate(bicubic_upscaled_batch):
            x = self.srcnn(x)
            x = torch.mean(x, dim=0)
            out.append(x)

        out = torch.stack(out, 0)

        return out