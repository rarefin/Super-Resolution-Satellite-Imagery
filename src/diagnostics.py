#!/usr/bin/env python
"""
Some utilities for diagnosing issues with training. This involves
- Inspecting weights and activations during training
- Performing error analysis
"""
import matplotlib.pyplot as plt
import os
import os.path
import pandas as pd
import tempfile
import torch

def output_dir(dirname):
    if not dirname:
        dirname = tempfile.mkdtemp()
    os.makedirs(dirname, exist_ok=True)
    return dirname


def stack(arr, cols=None):
    """
    :param arr: A multidimensional numpy array, which we will "melt" into tall
      form, to facilitate plotting.
    """
    if not cols:
        cols = ["channel_in", "channel_out", "w", "h", "weight"]

    index = pd.MultiIndex.from_product([range(s)for s in arr.shape])
    df = pd.DataFrame(
        {'value': arr.flatten()},
        index=index
    )
    df = df.reset_index()
    df.columns = cols
    return df


def write_weights(layers, dirname=None, prefix="filters"):
    """
    :param layers: A torch Sequential object, with a .conv term.
    """
    dirname = output_dir(dirname)
    for k in range(len(layers)):
        print("Writing layer {}".format(k))
        weights = stack(layers[k].conv.weight.data.numpy())
        output_path = os.path.join(
            dirname,
            "{}_depth-{}.csv".format(prefix, k)
        )
        weights.to_csv(output_path, index=False)

    return dirname


def write_activations(layers, batch, dirname=None, prefix="activation"):
    """
    :param layers: A torch Sequential object, whose activations we want to
      access.
    """
    dirname = output_dir(dirname)
    for k in range(len(layers)):
        print("Writing layer {}".format(k))

        maps = layers[:k](batch)
        for i in range(batch.shape[0]):
            map = stack(
                maps[i].detach().numpy(),
                ["filter", "w", "h", "activation"]
            )

            output_path = os.path.join(
                dirname,
                "{}_depth-{}_sample-{}.csv".format(prefix, k, i)
            )
            map.to_csv(output_path, index=False)

    return dirname
