import numpy as np
import os
import csv
from zipfile import ZipFile
from skimage import transform, io
import warnings
import matplotlib.pyplot as plt
import torch


def readImage(image_path):
    """
    :param image_path: path of the image
    :return: return image in numpy array
    """

    return io.imread(image_path)


def saveImage(image, output_path):
    """
    :param image: image in numpy array
    :param output_path: Output path of image
    :return:
    """
    io.imsave(output_path, image)


def expandDim(img):
    """
    Expanding axis of a single channel image
    :param img: Image
    :return:
    """
    return np.expand_dims(img, axis=0)


def readBaselineCPSNR(filePath):
    """

    :param filePath: path the baseline cPSNR file
    :return: base line cPSNR for each imageset in a list
    """
    baseline_cPSNRs = dict()
    with open(filePath, 'r') as file:
        reader = csv.reader(file, delimiter=' ')
        for row in reader:
            baseline_cPSNRs[row[0].strip()] = float(row[1].strip())

    return baseline_cPSNRs


def traditionalUpscaling(low_res_image, interpolation_method, upscaling_factor):
    """

    :param low_res_image: Lower resolution Images in numpy array : shape [height, width, channels]
    :param interpolation_method: Interpolation method name
    :param upscaling_factor: upscaling factor for upscaling the lower resolution image
    :return: Super Resolution Image based on traditional methods
    """

    interpolation_methods = ['NearestNeighbor', 'BiLinear', 'BiQuadratic', 'BiCubic', 'BiQuartic', 'BiQuintic', 'SRGAN']

    order = interpolation_methods.index(interpolation_method)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        super_res = transform.rescale(low_res_image, scale=upscaling_factor, order=order, preserve_range=True)

    return super_res


def getImageSetDirectories(data_directory):
    """
    :param data_directory: this is either train or test data directory
    :return: imageset directory list
    """

    imageSetDirectories = []
    for channel in ['RED', 'NIR']:
        path = os.path.join(data_directory, channel)
        for imageSetName in os.listdir(path):
            imageSetDirectories.append(os.path.join(path, imageSetName))

    return imageSetDirectories


def prepareSubmission(super_res_image_directory):
    # name of submission archive
    sub_archive = super_res_image_directory + '/submission.zip'

    zf = ZipFile(sub_archive, mode='w')
    try:
        for img in os.listdir(super_res_image_directory):
            # ignore the .zip-file itself
            if not img.startswith('imgset'):
                continue
            zf.write(super_res_image_directory + '/' + img, arcname=img)
            print('*', end='', flush='True')
    finally:
        zf.close()
    print('\ndone. The submission-file is found at {}. Bye!'.format(sub_archive))


def plotLoss(train_loss, val_loss):
    """

    :param train_loss: train losses in different epochs
    :param val_loss: validation losses in different epochs
    :return:
    """
    plt.yscale('log')
    plt.plot(train_loss)
    plt.plot(val_loss)
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'Validation'], loc='upper right')
    plt.show()


def plot_grad_flow(named_parameters):
    ave_grads = []
    layers = []
    for n, p in named_parameters:
        if (p.requires_grad) and ("bias" not in n):
            layers.append(n)
            ave_grads.append(p.grad.abs().mean())
    plt.plot(ave_grads, alpha=0.3, color="b")
    plt.hlines(0, 0, len(ave_grads) + 1, linewidth=1, color="k")
    plt.xticks(range(0, len(ave_grads), 1), layers, rotation="vertical")
    plt.xlim(xmin=0, xmax=len(ave_grads))
    plt.xlabel("Layers")
    plt.ylabel("average gradient")
    plt.title("Gradient flow")
    plt.grid(True)
    plt.show()


def equalizeDimension(batch, axis=0):
    _, channel, height, width = batch[0].size()
    num_low = [item.size()[axis] for item in batch]
    padded_batch = []
    for i, item in enumerate(batch):
        padded_item = torch.zeros((max(num_low), channel, height, width)).float()
        end = num_low[i]
        padded_item[:end] = item
        padded_batch.append(padded_item)
    return padded_batch


def collateFunction(batch):
    """
        Create custom colate function to adjust variable number of low res images. data are in pytorch tensor of shape
        [batch, num_low_res, channel, height, width]
    """
    lr_batch = []
    bu_batch = []
    ls_batch = []
    hr_batch = []
    hs_batch = []
    isn_batch = []
    for data in batch:
        low_res_all, bicubic_upscaled_all, low_status_all, high_res_image, high_res_status_map, image_set_name = data
        lr_batch.append(low_res_all)
        bu_batch.append(bicubic_upscaled_all)
        ls_batch.append(low_status_all)
        hr_batch.append(high_res_image)
        hs_batch.append(high_res_status_map)
        isn_batch.append(image_set_name)

    hr_batch = torch.stack(hr_batch, 0)
    hs_batch = torch.stack(hs_batch, 0)

    return lr_batch, bu_batch, ls_batch, hr_batch, hs_batch, isn_batch




