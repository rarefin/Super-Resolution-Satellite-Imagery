import os
from glob import glob
import numpy as np
import random
from skimage import transform, filters, util
from .utils import readImage, saveImage, createDirectory, traditionalUpscaling


def addRandomNoise(image, mean=0, std=0.01):
    """
    Add gaussian nosie to the input image
    :param image: input image
    :param mean: mean of the distribution
    :param std: std of the distribution
    :return: image with added noise
    """
    image_with_random_noise = util.random_noise(image, mode='gaussian', mean=mean, var=std)

    return image_with_random_noise


def rotate(image, angle=180):
    """
    Rotate image to a particular orientation
    :param image: input image
    :param angle: output image orientation
    :return: rotated image
    """
    image_with_rotation = transform.rotate(image, angle)

    return image_with_rotation


def blur(image, std=0.03):
    """
    Smoothing the input image

    :param original_image: input image
    :param std: gaussian kernel siz
    :return:
    """
    blurred_image = filters.gaussian(image, sigma=std, mode='nearest', \
                                     multichannel=False, preserve_range=False)

    return blurred_image


def translate(image, tx=10, ty=10):
    """

    :param image: input image
    :param: tx: translate tx quantity in x axis
    :param: ty: translate ty quantity in y axis
    :return: translated image
    """
    tr_translation = transform.SimilarityTransform(translation=[tx, ty])

    translated_image = transform.warp(image, tr_translation.inverse)

    return translated_image


def warp(original_image, orientation=15, tx=5, ty=5):

    """
    Warp the image by translation and rotation
        :param: original_image: input image
        :param: orientation: orientation of rotated image
        :param: tx: translate tx quantity in x axis
        :param: ty: translate ty quantity in y axis

        :return: warped, bicubic iterpolated image
    """

    tr_rotation = transform.SimilarityTransform(rotation=np.deg2rad(orientation))
    tr_translation = transform.SimilarityTransform(translation=[tx, ty])
    warped_image = transform.warp(original_image, (tr_rotation + tr_translation).inverse, \
                                  order=3, preserve_range=False)

    return warped_image


def getRandomValue(min, max):
    """
    Get random value in a range used for metadata of translation and rotation
    :param min: min value of range
    :param max: max value of range
    :return: metadata
    """
    return random.randint(min, max)


def generateMockData(input_directory, generated_data_directory, num_random_views=8):
    """
    Warp the HR images by some transformation and generate multiple lower res images and save the metadata
    in the lower res image names
    :param input_directory: input data directory
    :param generated_data_directory: output data directory
    :param num_random_views: no of random views
    :return:
    """

    for file_path in glob(os.path.join(input_directory, '*')):
        dir = file_path.split('/')[-1].split('.')[0]
        createDirectory(os.path.join(generated_data_directory, dir))
        HR = readImage(file_path)
        saveImage(HR, os.path.join(generated_data_directory, dir, 'HR.png'))
        for i in range(num_random_views):
            tx = getRandomValue(min=-5, max=5)
            ty = getRandomValue(min=-5, max=5)
            orientation = getRandomValue(min=-10, max=10)

            HR = blur(HR, std=0.01)
            HR = warp(HR, 2, orientation, tx, ty)
            LR = traditionalUpscaling(HR, 'BiCubic', upscaling_factor=1/2)
            saveImage(LR, os.path.join(generated_data_directory, dir, \
                                       'LR_{}_ori_{}_tx_{}_ty_{}.png'.format(i, orientation, tx, ty)))