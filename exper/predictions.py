"""
Small script to save truth / predictions to file
"""

from Model import Model
from utils import getImageSetDirectories, prepareSubmission, readBaselineCPSNR, plotLoss
import os
from DataLoader import SingleImageDataset, AllImageDataset
from torch.utils.data import DataLoader
import numpy as np
from DeepNetworks.VDSR import VDSR
from torch import nn
import torch.optim as optim
from torchvision import transforms
from Transforms import ToTensor, MinMaxNormalize
import torch
import random
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import sys
sys.path.append("../src/")

data_directory = '/Users/krissankaran/Downloads/probav/'
model_path = "/Users/krissankaran/Downloads/SRCNN.pth"
save_path = "/Users/krissankaran/Desktop/"
output_directory = os.path.join(data_directory, "output")
baseline_cpsnrs = readBaselineCPSNR(os.path.join(data_directory, 'norm.csv'))
train_set_directories = getImageSetDirectories(os.path.join(data_directory, "train"))
test_set_directories = getImageSetDirectories(os.path.join(data_directory, "test"))


network = VDSR(in_channels=1, out_channels=1)
model = Model(network)
val_proportion = 0.10
train_list, val_list = train_test_split(train_set_directories, test_size=val_proportion, random_state=1, shuffle=True)

transform = transforms.Compose([
        ToTensor(),
        MinMaxNormalize()
    ])
train_dataset = SingleImageDataset(image_set_directories=train_list, isClearest=True, transform=transform)
train_dataloader = DataLoader(train_dataset, batch_size=1, shuffle=True, num_workers=4)

model.model.load_state_dict(torch.load(model_path, map_location="cpu"))

for x, x_bic, x_stat, y, _, name in train_dataloader:
    y_hat = model.model((x, x_bic, x_stat))
    np.savetxt("{}/x_{}.csv".format(save_path, name[0]), x_bic[0, 0, :, :].detach().numpy())
    np.savetxt("{}/pred_{}.csv".format(save_path, name[0]), y_hat[0, 0, :, :].detach().numpy())
    np.savetxt("{}/truth_{}.csv".format(save_path, name[0]), y[0, 0, :, :].detach().numpy())
