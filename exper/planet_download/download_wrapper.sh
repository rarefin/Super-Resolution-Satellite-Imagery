#!/usr/bin/env bash

for (( k = 0; k < 50; ++k )); do
    a=$(( 10*k + 1 ))
    bash planet_downloads.batch $PLANET_API_KEY $a
done
