
source("../../src/diagnostics.R")
weights_dir <- "/var/folders/43/w0wvg7pd2p7d651g7rz0pj8r0000gn/T/tmp6j4s5plz/" # printed in run_diagnostics.py
activations_dir <- "/var/folders/43/w0wvg7pd2p7d651g7rz0pj8r0000gn/T/tmpr84f_j4n/"
output_dir <- "~/Desktop/"
plot_wrapper(weights_dir, weights_hm, output_dir, "weights_map")
plot_wrapper(weights_dir, weights_histo, output_dir, "weights_histo")
plot_wrapper(activations_dir, activations_hm, output_dir, "activations_hm")
plot_wrapper(activations_dir, activations_histo, output_dir, "activations_histo")
