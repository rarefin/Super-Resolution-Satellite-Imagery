#!/usr/bin/env python
"""
A gross script to write data for weights / activations (for a single image) for
some random model. Really only kept for the sake of reproducibility.
"""
import sys
sys.path.append("../../src/DeepNetworks/")
sys.path.append("../../src/")
from VDSR import VDSR
from diagnostics import *

# write weights. Even though the file is called SRCNN.pth, it's a VDSR network.
model = VDSR(1, 1)
model = model.cpu().double()
model.load_state_dict(torch.load("/Users/krissankaran/Downloads/SRCNN.pth", map_location="cpu"))
weight_dir = write_weights(model.residual_layers)
print(weight_dir)

# libraries for getting a batch
from utils import getImageSetDirectories, prepareSubmission, readBaselineCPSNR, plotLoss
import os
from DataLoader import SingleImageDataset, AllImageDataset
from torchvision import transforms
from Transforms import ToTensor, MinMaxNormalize
from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split

# extract a batch
data_directory = "/Users/krissankaran/Downloads/probav/"
train_set_directories = getImageSetDirectories(os.path.join(data_directory, "train"))
val_proportion = 0.10
train_list, _ = train_test_split(train_set_directories, test_size=val_proportion, random_state=1, shuffle=True)
transform = transforms.Compose([ToTensor(), MinMaxNormalize()])
train_dataset = SingleImageDataset(image_set_directories=train_list, isClearest=True, transform=transform)
train_dataloader = DataLoader(train_dataset, batch_size=1, shuffle=True, num_workers=4)

# write all the activations. This can take a few minutes.
(low_res_batch, bicubic_upscaled_batch, low_res_status_batch, _, _, _) = next(iter(train_dataloader))
residual = model.relu(model.input(bicubic_upscaled_batch.double()))
activations_dir = write_activations(model.residual_layers, residual)
print(activations_dir)
